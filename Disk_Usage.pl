#!/usr/bin/perl
# Perl Program to Check the size of /TivoData and / partition in all Download servers.
# Provided the servers need to be specified in servers.txt file
# Developed by Nikhil Narayanan on Oct 16th 2017
# Happy Coading........

# Importing Module
use strict;
use warnings;
use Net::SSH::Perl;
use Term::ReadKey;
use Net::SSH::Perl::Key qw(read_private);
use Mail::Sendmail;

# Reduce output bufferring
$|=1;

# Declaring public array's
my (@push,@push1,@push2);

# Main function calls
my ($val_Ref1,$val_Ref2,$val_Ref3) = do_ssh();
send_mail(@{$val_Ref1},@{$val_Ref2},@{$val_Ref3});

# Subroutine to do SSH
sub do_ssh
        {
        my $file = 'servers.txt';
        open(INPUT,$file) or die("File not found. \n");

        my $cmd = 'df -h';
        my $username = 'tivo';
        while(my $host = <INPUT>) {
                chomp($host);
                my $ssh = Net::SSH::Perl->new($host, protocol => '2,1');
                my $keypath = '/Users/nnarayanan/.ssh/spkeypri_dsa';
                my $key = Net::SSH::Perl::Key->read_private('DSA', $keypath , [ '']);

                # Starting login
                $ssh->login($username);

                # Starting command
                my($stdout, $stderr, $exit) = $ssh->cmd($cmd);
                        if ( $stdout =~ /\s(\d{2,3}\w)+\s+(\d{2,3})\%+\s+(\/T\w*a)/ ) {
                                if ( $2 >= 95 ) {
                                        push @push, "$host: \t $3 \t Usage: $2% \t Free: $1\n";
                                }
                                else {
                                        push @push1, "$host: \t $3 \t Usage: $2% \t Free: $1\n";
                                }
                        }
			if ( $stdout =~ /\s(\d{2,3})\%+\s+(\/)/) {
                                        push @push2, "$host: \t Usage: $1% \n";
                                }
        }
	return(\@push,\@push1,\@push2);
};

# Subroutine to send mail
sub send_mail {
	    my %mail = ( To => 'TiVo-India-IT-TechOps@tivo.com',
			 From => 'nikhil.narayanan@tivo.com',
                         Subject => 'Disk Usage In Download Servers',
			 Message => "Above 95%:\n @{$val_Ref1}\n Below 95%:\n @{$val_Ref2}\n\n Root Partition ( / ) Usage:\n @{$val_Ref3}\n",
                       );
            sendmail(%mail) or die $Mail::Sendmail::error;
            print "Mail Sent \n";
};
